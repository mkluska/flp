/**
 * Project: Rubik's cube solver in Prolog
 * Author:  Martin Kluska <xklusk00@stud.fit.vutbr.cz>
 *
 * Solver using own implementation of BFS algorithm
 */

solve(C, Goal) :-
	solveBfsInternal([([], C)], Goal, [], C).

solveBfsInternal([(Moves, Goal) | _], Goal, _, C) :-
	reverse(Moves, M),
	writeSolutions(M, C).

solveBfsInternal([(Moves, Cube) | REST], Goal, Close, C) :-
	rotate(u, Cube, U),
	rotate(l, Cube, L),
	rotate(f, Cube, F),
	rotate(r, Cube, R),
	rotate(b, Cube, B),
	rotate(d, Cube, D),
	rotate(ui, Cube, UI),
	rotate(li, Cube, LI),
	rotate(fi, Cube, FI),
	rotate(ri, Cube, RI),
	rotate(bi, Cube, BI),
	rotate(di, Cube, DI),

	bfsAddIfNotExists(REST, ([u|Moves],  U),  O1,  Close),
	bfsAddIfNotExists(O1,   ([l|Moves],  L),  O2,  Close),
	bfsAddIfNotExists(O2,   ([f|Moves],  F),  O3,  Close),
	bfsAddIfNotExists(O3,   ([r|Moves],  R),  O4,  Close),
	bfsAddIfNotExists(O4,   ([b|Moves],  B),  O5,  Close),
	bfsAddIfNotExists(O5,   ([d|Moves],  D),  O6,  Close),
	bfsAddIfNotExists(O6,   ([ui|Moves], UI), O7,  Close),
	bfsAddIfNotExists(O7,   ([li|Moves], LI), O8,  Close),
	bfsAddIfNotExists(O8,   ([fi|Moves], FI), O9,  Close),
	bfsAddIfNotExists(O9,   ([ri|Moves], RI), O10, Close),
	bfsAddIfNotExists(O10,  ([bi|Moves], BI), O11, Close),
	bfsAddIfNotExists(O11,  ([di|Moves], DI), O12, Close),

	solveBfsInternal(O12 , Goal, [Cube, Close], C).

bfsAddIfNotExists(RESULT, (_, X), RESULT, CLOSE) :-
	member(X, CLOSE).

bfsAddIfNotExists(Q, X, RESULT, _) :-
	append(Q, [X], RESULT).

member(H,[H|_]).
member(H,[_|T]):-
	member(H,T).
