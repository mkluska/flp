/**
  * Project: Rubik's cube solver in Prolog
  * Author:  Martin Kluska <xklusk00@stud.fit.vutbr.cz>
  *
  * Solver using prolog unification
  */

solve(C, Goal) :-
	solveInternal(Solutions, C, Goal),
	reverse(Solutions, S),
	writeSolutions(S, C).

solveInternal([], C, C). % in goal state
solveInternal([H|T], C, Goal) :- 
	solveInternal(T, C, D), 
	rotate(H, D, Goal).

getBlockHeurestic(X, X, 1).
getBlockHeurestic(_, _, 0).

getSideHeurestic(side(A1, A2, A3, A4, A5, A6, A7, A8, A9), Result) :-
	getBlockHeurestic(A1, A5, R1),
	getBlockHeurestic(A2, A5, R2),
	getBlockHeurestic(A3, A5, R3),
	getBlockHeurestic(A4, A5, R4),
	getBlockHeurestic(A6, A5, R6),
	getBlockHeurestic(A7, A5, R7),
	getBlockHeurestic(A8, A5, R8),
	getBlockHeurestic(A9, A5, R9),
	Result is R1 + R2 + R3 + R4 + R6 + R7 + R8 + R9.

getCubeHeurestic(cube(
	F1, F2, F3, F4, F5, F6, F7, F8, F9,
	R1, R2, R3, R4, R5, R6, R7, R8, R9,
	B1, B2, B3, B4, B5, B6, B7, B8, B9,
	L1, L2, L3, L4, L5, L6, L7, L8, L9,
	U1, U2, U3, U4, U5, U6, U7, U8, U9,
	D1, D2, D3, D4, D5, D6, D7, D8, D9
), Result) :-
	getSideHeurestic(side(F1, F2, F3, F4, F5, F6, F7, F8, F9), X1),
	getSideHeurestic(side(R1, R2, R3, R4, R5, R6, R7, R8, R9), X2),
	getSideHeurestic(side(B1, B2, B3, B4, B5, B6, B7, B8, B9), X3),
	getSideHeurestic(side(L1, L2, L3, L4, L5, L6, L7, L8, L9), X4),
	getSideHeurestic(side(U1, U2, U3, U4, U5, U6, U7, U8, U9), X5),
	getSideHeurestic(side(D1, D2, D3, D4, D5, D6, D7, D8, D9), X6),
	Result is X1 + X2 + X3 + X4 + X5 + X6.
