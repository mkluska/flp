# define solver algoritm
# possible values are
#
# 	solver.pl    - for Prolog solver
#	solverBFS.pl - for BFS solver

SOLVER=solver.pl
SOURCE_FILES=main.pl reader.pl writer.pl rotations.pl $(SOLVER)
NAME=flp17-log

$(NAME):
	swipl -q -g main -o $(NAME) -c $(SOURCE_FILES)

test: $(NAME)
	./test.sh

clean:
	rm -f *.hi *.o *$(NAME).tgz $(NAME)

pack: clean
	zip flp-log-xklusk00.zip *.pl Makefile README tests/
