/**
  * Project: Rubik's cube solver in Prolog
  * Author:  Martin Kluska <xklusk00@stud.fit.vutbr.cz>
  */

/**
  * Write long line.
  * IE Lines 4 to 6 from input file
  */
writeLine(A1, A2, A3, A4, A5, A6, A7, A8, A9, A10, A11, A12) :- 
      writeLine(A1, A2, A3), write(' '),
      writeLine(A4, A5, A6), write(' '),
      writeLine(A7, A8, A9), write(' '),
      writeLine(A10, A11, A12).

/**
  * Write short line.
  * IE Lines 1 to 3 and 7 to 9 from input file
  */
writeLine(A1, A2, A3) :- 
      write(A1), write(A2), write(A3).

/**
  * Write cube to output
  */
writeCube(cube(
	F1, F2, F3, F4, F5, F6, F7, F8, F9,
	R1, R2, R3, R4, R5, R6, R7, R8, R9,
	B1, B2, B3, B4, B5, B6, B7, B8, B9,
	L1, L2, L3, L4, L5, L6, L7, L8, L9,
	U1, U2, U3, U4, U5, U6, U7, U8, U9,
	D1, D2, D3, D4, D5, D6, D7, D8, D9
)) :- 
      writeLine(U1, U2, U3), write('\n'),
      writeLine(U4, U5, U6), write('\n'),
      writeLine(U7, U8, U9), write('\n'),
      
      writeLine(F1, F2, F3, R1, R2, R3, B1, B2, B3, L1, L2, L3), write('\n'),
      writeLine(F4, F5, F6, R4, R5, R6, B4, B5, B6, L4, L5, L6), write('\n'),
      writeLine(F7, F8, F9, R7, R8, R9, B7, B8, B9, L7, L8, L9), write('\n'),

      writeLine(D1, D2, D3), write('\n'),
      writeLine(D4, D5, D6), write('\n'),
      writeLine(D7, D8, D9), write('\n').

/**
  * Dump all solutions
  */
writeSolutions([], C) :- 
      writeCube(C).

writeSolutions([H|T], C) :- 
      % write current state
      writeCube(C), 

      % new line between states
      write('\n'), 

      % calculate new state
      rotate(H, C, D), 

      % write new state
      writeSolutions(T, D). 
