/**
 * Project: Rubik's cube solver in Prolog
 * Author:  Martin Kluska <xklusk00@stud.fit.vutbr.cz>
 *
 * Main module of solver
 */


/**
  * Prototype of solved cube.
  * Solved cube has items of same value in each line
  */
solved(cube(
		F, F, F, F, F, F, F, F, F,
		R, R, R, R, R, R, R, R, R,
		B, B, B, B, B, B, B, B, B,
		L, L, L, L, L, L, L, L, L,
		U, U, U, U, U, U, U, U, U,
		D, D, D, D, D, D, D, D, D
	)).

main :- 
	prompt(_, ''),
	
	%read input cube
	readCube(C), 

	% prepare prototype of goal state
	solved(Goal), 

	% calculate steps
	solve(C, Goal), 

	halt(0).
