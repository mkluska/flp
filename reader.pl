/**
 * Project: Rubik's cube solver in Prolog
 * Author:  Martin Kluska <xklusk00@stud.fit.vutbr.cz>
 *
 * Read cube from input stream
 */

readCubeLine(A1, A2, A3, A4, A5, A6, A7, A8, A9, A10, A11, A12) :-
     readCubeLine(A1, A2, A3),
     readCubeLine(A4, A5, A6),
     readCubeLine(A7, A8, A9),
     readCubeLine(A10, A11, A12).

readCubeLine(A1, A2, A3) :-
     get_char(user_input, A1), 
     get_char(user_input, A2), 
     get_char(user_input, A3), 
     get_char(user_input, _).

readCube(cube(
	F1, F2, F3, F4, F5, F6, F7, F8, F9,
	R1, R2, R3, R4, R5, R6, R7, R8, R9,
	B1, B2, B3, B4, B5, B6, B7, B8, B9,
	L1, L2, L3, L4, L5, L6, L7, L8, L9,
	U1, U2, U3, U4, U5, U6, U7, U8, U9,
	D1, D2, D3, D4, D5, D6, D7, D8, D9
)) :-
	readCubeLine(U1, U2, U3),
	readCubeLine(U4, U5, U6),
	readCubeLine(U7, U8, U9),

	readCubeLine(F1, F2, F3, R1, R2, R3, B1, B2, B3, L1, L2, L3),
	readCubeLine(F4, F5, F6, R4, R5, R6, B4, B5, B6, L4, L5, L6),
	readCubeLine(F7, F8, F9, R7, R8, R9, B7, B8, B9, L7, L8, L9),

	readCubeLine(D1, D2, D3),
	readCubeLine(D4, D5, D6),
	readCubeLine(D7, D8, D9).
