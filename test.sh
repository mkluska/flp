RED='\033[0;31m'
GREEN='\033[0;32m'
YEL='\033[0;33m'
NC='\033[0m' # No Color
total=0
ok=0

echo "Starting testing..."
echo "***********************************************"

for (( i=1; i<=13; i++ ))
do
	((total++))

	./flp17-log < tests/test$i/input.txt > tests/test$i/output.txt
	diff tests/test$i/expected.txt tests/test$i/output.txt
	if [ "$?" == "0" ]
		then
			echo -e "[TEST $i] ${GREEN}OK${NC}"
			((ok++))
			rm tests/test$i/output.txt
		else
			echo -e "[TEST $i] ${RED}BAD${NC}"
		fi
		echo -e "\033[0m-----------"
done

echo "***********************************************"
echo -e "FINISHED TESTING:"
echo -e "OK/TOTAL: ${ok}/${total}"

if [ "$ok" == "$total" ]
  then
    echo -e "[${GREEN}SUCCESSFUL${NC}]"
   else
    echo -e "[${RED}FAILED${NC}]"
fi