/**
 * Project: Rubik's cube solver in Prolog
 * Author:  Martin Kluska <xklusk00@stud.fit.vutbr.cz>
 *
 * Cube rotations according to https://ruwix.com/the-rubiks-cube/algorithm/
 */


/**
 *  Rotate cube Up
 *
 *  ===From===            ===To===
 *
 *  555                   555
 *  555                   555
 *  555                   555
 *  111 222 333 444       222 333 444 111
 *  111 222 333 444       111 222 333 444
 *  111 222 333 444       111 222 333 444
 *  666                   666
 *  666                   666
 *  666                   666
 */
rotate(u,
	cube(
		F1, F2, F3, F4, F5, F6, F7, F8, F9,
		R1, R2, R3, R4, R5, R6, R7, R8, R9,
		B1, B2, B3, B4, B5, B6, B7, B8, B9,
		L1, L2, L3, L4, L5, L6, L7, L8, L9,
		U1, U2, U3, U4, U5, U6, U7, U8, U9,
		D1, D2, D3, D4, D5, D6, D7, D8, D9
	), 
	cube(
		R1, R2, R3, F4, F5, F6, F7, F8, F9, %swap items
		B1, B2, B3, R4, R5, R6, R7, R8, R9, %swap items
		L1, L2, L3, B4, B5, B6, B7, B8, B9, %swap items
		F1, F2, F3, L4, L5, L6, L7, L8, L9, %swap items
		U7, U4, U1, U8, U5, U2, U9, U6, U3, %rotate
		D1, D2, D3, D4, D5, D6, D7, D8, D9  %keep same
	)
).

/**
 *  Rotate cube Left
 *
 *  ===From===            ===To===
 *
 *  555                   355
 *  555                   355
 *  555                   355
 *  111 222 333 444       511 222 336 444
 *  111 222 333 444       511 222 336 444
 *  111 222 333 444       511 222 336 444
 *  666                   166
 *  666                   166
 *  666                   166
 */
rotate(l,
	cube(
		F1, F2, F3, F4, F5, F6, F7, F8, F9,
		R1, R2, R3, R4, R5, R6, R7, R8, R9,
		B1, B2, B3, B4, B5, B6, B7, B8, B9,
		L1, L2, L3, L4, L5, L6, L7, L8, L9,
		U1, U2, U3, U4, U5, U6, U7, U8, U9,
		D1, D2, D3, D4, D5, D6, D7, D8, D9
	), 
	cube(
		U1, F2, F3, U4, F5, F6, U7, F8, F9,
		R1, R2, R3, R4, R5, R6, R7, R8, R9,
		B1, B2, D7, B4, B5, D4, B7, B8, D1,
		L7, L4, L1, L8, L5, L2, L9, L6, L3,
		B9, U2, U3, B6, U5, U6, B3, U8, U9,
		F1, D2, D3, F4, D5, D6, F7, D8, D9
	)
).

/**
 *  Rotate cube Front
 *
 *  ===From===            ===To===
 *
 *  555                   555
 *  555                   555
 *  555                   444
 *  111 222 333 444       111 522 333 446
 *  111 222 333 444       111 522 333 446
 *  111 222 333 444       111 522 333 446
 *  666                   222
 *  666                   666
 *  666                   666
 */
rotate(f,
	cube(
		F1, F2, F3, F4, F5, F6, F7, F8, F9,
		R1, R2, R3, R4, R5, R6, R7, R8, R9,
		B1, B2, B3, B4, B5, B6, B7, B8, B9,
		L1, L2, L3, L4, L5, L6, L7, L8, L9,
		U1, U2, U3, U4, U5, U6, U7, U8, U9,
		D1, D2, D3, D4, D5, D6, D7, D8, D9
	), 
	cube(
		F7, F4, F1, F8, F5, F2, F9, F6, F3,
		U7, R2, R3, U8, R5, R6, U9, R8, R9,
		B1, B2, B3, B4, B5, B6, B7, B8, B9,
		L1, L2, D1, L4, L5, D2, L7, L8, D3,
		U1, U2, U3, U4, U5, U6, L9, L6, L3,
		R7, R4, R1, D4, D5, D6, D7, D8, D9
	)
).

/**
 *  Rotate cube right
 *
 *  ===From===            ===To===
 *
 *  555                   551
 *  555                   551
 *  555                   551
 *  111 222 333 444       116 222 533 444
 *  111 222 333 444       116 222 533 444
 *  111 222 333 444       116 222 533 444
 *  666                   663
 *  666                   663
 *  666                   663
 */
rotate(r,
	cube(
		F1, F2, F3, F4, F5, F6, F7, F8, F9,
		R1, R2, R3, R4, R5, R6, R7, R8, R9,
		B1, B2, B3, B4, B5, B6, B7, B8, B9,
		L1, L2, L3, L4, L5, L6, L7, L8, L9,
		U1, U2, U3, U4, U5, U6, U7, U8, U9,
		D1, D2, D3, D4, D5, D6, D7, D8, D9
	), 
	cube(
		F1, F2, D3, F4, F5, D6, F7, F8, D9,
		R7, R4, R1, R8, R5, R2, R9, R6, R3,
		U9, B2, B3, U6, B5, B6, U3, B8, B9,
		L1, L2, L3, L4, L5, L6, L7, L8, L9,
		U1, U2, F3, U4, U5, F6, U7, U8, F9,
		D1, D2, B7, D4, D5, B4, D7, D8, B1
	)
).

/**
 *  Rotate cube back
 *
 *  ===From===            ===To===
 *
 *  555                   222
 *  555                   555
 *  555                   555
 *  111 222 333 444       111 226 333 544
 *  111 222 333 444       111 226 333 544
 *  111 222 333 444       111 226 333 544
 *  666                   666
 *  666                   666
 *  666                   444
 */
rotate(b,
	cube(
		F1, F2, F3, F4, F5, F6, F7, F8, F9,
		R1, R2, R3, R4, R5, R6, R7, R8, R9,
		B1, B2, B3, B4, B5, B6, B7, B8, B9,
		L1, L2, L3, L4, L5, L6, L7, L8, L9,
		U1, U2, U3, U4, U5, U6, U7, U8, U9,
		D1, D2, D3, D4, D5, D6, D7, D8, D9
	), 
	cube(
		F1, F2, F3, F4, F5, F6, F7, F8, F9,
		R1, R2, D9, R4, R5, D8, R7, R8, D7,
		B7, B4, B1, B8, B5, B2, B9, B6, B3,
		U3, L2, L3, U2, L5, L6, U1, L8, L9,
		R3, R6, R9, U4, U5, U6, U7, U8, U9,
		D1, D2, D3, D4, D5, D6, L1, L4, L7
	)
).

/**
 *  Rotate cube down
 *
 *  ===From===            ===To===
 *
 *  555                   555
 *  555                   555
 *  555                   555
 *  111 222 333 444       111 222 333 444
 *  111 222 333 444       111 222 333 444
 *  111 222 333 444       444 111 222 333
 *  666                   666
 *  666                   666
 *  666                   666
 */
rotate(d,
	cube(
		F1, F2, F3, F4, F5, F6, F7, F8, F9,
		R1, R2, R3, R4, R5, R6, R7, R8, R9,
		B1, B2, B3, B4, B5, B6, B7, B8, B9,
		L1, L2, L3, L4, L5, L6, L7, L8, L9,
		U1, U2, U3, U4, U5, U6, U7, U8, U9,
		D1, D2, D3, D4, D5, D6, D7, D8, D9
	), 
	cube(
		F1, F2, F3, F4, F5, F6, L7, L8, L9,
		R1, R2, R3, R4, R5, R6, F7, F8, F9,
		B1, B2, B3, B4, B5, B6, R7, R8, R9,
		L1, L2, L3, L4, L5, L6, B7, B8, B9,
		U1, U2, U3, U4, U5, U6, U7, U8, U9,
		D7, D4, D1, D8, D5, D2, D9, D6, D3
	)
).

/**
  * Inverse up
  */
rotate(ui, C, D) :- rotate(u, D, C).

/**
  * Inverse left
  */
rotate(li, C, D) :- rotate(l, D, C).

/**
  * Inverse front
  */
rotate(fi, C, D) :- rotate(f, D, C).

/**
  * Inverse Right
  */
rotate(ri, C, D) :- rotate(r, D, C).

/**
  * Inverse back
  */
rotate(bi, C, D) :- rotate(b, D, C).

/**
  * Inverse down
  */
rotate(di, C, D) :- rotate(d, D, C).
